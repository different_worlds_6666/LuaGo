AndroLuaX+

AndroLuaX+是基于Androlua+开发的支持Google Material与AndroidX AppCompat控件及类库，颜色选择器ColorPicker，Google推荐的Glide图片加载框架，AgentWeb一个高度封装的WebView支持库(AndroLua+永远滴神，二改参考的话，请勿去除AndroLua+作者nirenr的名字)

AndroLua+是由nirenr开发的在安卓使用Lua语言开发应用的工具，该项目基于开源项目luajava和AndroLua优化加强，修复了原版的bug，并加入了很多新的特性，使开发更加简单高效，使用该软件完全免费，如果你喜欢这个项目欢迎捐赠或者宣传他。
在使用之前建议详细阅读程序自带帮助文档。
用户协议
作者不对使用该软件产生的任何直接或间接损失负责。
勿使用该程序编写恶意程序以损害他人。
继续使用表示你已知晓并同意该协议。

AndroLuaX+交流群：762838526

项目主页 地址:https://different_worlds_6666.gitee.io/luago/

本程序使用了以下开源项目部分代码

bson,crypt,md5
https://github.com/cloudwu/skynet

cjson
https://sourceforge.net/projects/cjson/

zlib
https://github.com/brimworks/lua-zlib

xml
https://github.com/chukong/quick-cocos2d-x

luv
https://github.com/luvit/luv
https://github.com/clibs/uv

zip
https://github.com/brimworks/lua-zip
https://github.com/julienr/libzip-android

luagl
http://luagl.sourceforge.net/

luasocket
https://github.com/diegonehab/luasocket

sensor
https://github.com/ddlee/AndroidLuaActivity

canvas
由落叶似秋开发

jni
由nirenr开发

AndroLua+：
https://github.com/nirenr/AndroLua_pro
由nirenr开发

Google推荐的Glide图片加载框架:
https://github.com/bumptech/Glide


颜色选择器:
https://github.com/QuadFlask/ColorPicker

AgentWeb是一个高度封装的WebView支持库:
https://github.com/Justson/AgentWeb